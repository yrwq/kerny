## Introduction

<p align="center">
<img src="https://ForTheBadge.com/images/badges/made-with-python.svg">
<br>
<img src="https://img.shields.io/badge/PRs-welcome-purple.svg?style=for-the-badge">
</p>

Kerny is just another Discord bot ... Written in python, for your shitty discord server!

## Features

- [x] Fetch (like neofetch but inside discord)
- [x] Wallpaper (random wallpaper from unslash, using queries)
- [x] Music (play songs from youtube, using youtubedl)

## Usage

### Setting up

1. If you don't already have one, create a new [Discord Application](https://discord.com/developers/applications).

2. Clone this repository.

```bash
git clone https://github.com/yrwq/kerny
```

3. Install python dependencies.

```bash
pip install -r requirements.txt
```

4. Edit `bot.py` to match your token & add your Discord ID to devs list.

## Contributing

Any contributions are very welcome, and feel free to make issues, i can't fix things i don't know about!
